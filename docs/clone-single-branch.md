

## 克隆单独分支  

本地新建一个空文件夹，此列是在d:\ 盘新建了test文件夹  

然后克隆单独分支 , 注意要在test文件夹下操作  

> git clone -b stevenmjh --single-branch https://gitlab.com/101camp/11py/tasks.git  

运行结果如下：  

```
D:\test                                                                                    
	λ git clone -b stevenmjh --single-branch https://gitlab.com/101camp/11py/tasks.git         
	Cloning into 'tasks'...                                                                    
	remote: Enumerating objects: 14, done.                                                     
	remote: Counting objects: 100% (14/14), done.                                              
	remote: Compressing objects: 100% (13/13), done.                                           
	remote: Total 143 (delta 1), reused 0 (delta 0), pack-reused 129                           
	Receiving objects: 100% (143/143), 61.21 KiB | 366.00 KiB/s, done.                         
	Resolving deltas: 100% (33/33), done.
```

检查一下有没有成功  

```
	D:\test                                                                                    
	λ ls -a                                                                                    
	./  ../  tasks/                                                                            
																	   
	D:\test                                                                                    
	λ git status                                                                               
	fatal: not a git repository (or any of the parent directories): .git 
```

提示：不是一个git 仓库。为咩呢 ？  

看看前面克隆时的显示：Cloning into 'tasks'

```
D:\test                                                                                    
	λ git clone -b stevenmjh --single-branch https://gitlab.com/101camp/11py/tasks.git         
	Cloning into 'tasks'...    
```

说明 tasks才是仓库文件夹。  

不信来检查一下。  

进入tasks文件夹。


```

	D:\test                                                                                    
	λ cd tasks                                                                     
	
    D:\test\tasks (stevenmjh -> origin)                                                        
	λ ls -a                                                                                    
	./  ../  .git/  .gitignore  PoL/  project/  README.md   
	
	D:\test\tasks (stevenmjh -> origin)                                                        
	λ git status                                                                               
	On branch stevenmjh                                                                        
	Your branch is up to date with 'origin/stevenmjh'.                                         
																							   
	nothing to commit, working tree clean                                                      
																							   
	D:\test\tasks (stevenmjh -> origin)                                                        
	λ git branch -a                                                                            
	* stevenmjh                                                                                
	  remotes/origin/stevenmjh                                                                 																   
```

我们看下里面有什么文件，发现有一个 .git的文件夹，这是仓库文件夹的标志之一  

再看状态status， 显示是在stevenmjh分支上，说明这个分支已经克隆下来了  

在用branch -a 查看一下所有分支，发现只有stevenmjh一个分支。说明克隆单独分支到本地成功。