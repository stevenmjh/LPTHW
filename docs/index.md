## 笨笨队The team of LPTHW  

### 团队介绍  

DAMA赐名：**笨笨队**  
英文简称：**LPTHW**    
队员：  @stevenmjh @icex101 @luluzh @lele1989 @mikecheng1992     
  
  **各尽所能，一起动手，团队协作，愉快玩耍**   


### 组员信息  

**Steven：** 主要在上海；海运行业；对Python有兴趣（大学期间学过一段时间编程语言，觉得有用，能解决工作中的问题，提高效率。  
**越越：**<常驻地；从事行业；来101camp的源动力。>  
**Lulu：**苏州；医药行业-质量；日常工作中有不了解IT语言/工作中遇到IT相关问题不知道解决办法是否正确的痛点，希望学习一门IT语言，方便日常工作。  
**乐乐：**浙江丽水；医药包装-供应链；希望学新的技能支持本职工作-供应链系统（数据处理）。  
**冰冰（icex）：**南京；人力资源（HR)；HR系统应用-方便与技术沟通，希望更智能化的工具替代excel。  
**Mike：**杭州（研究生-化工专业）；希望未来工作和数据结合。  

## 项目概要   

> Automail web app 
>
> 做一个可以自动发送生日祝福邮件的web应用

## 协作开发的几点约定  

- 在本地仓库复本开始工作之前先pull, 同步远程仓库的最新版本下来
- push到远程仓库的文件是代码、纯文本文档、数据文件
  - 避免上传图片、音视频、word文档等
- 记笔记，记笔记，记笔记

## 知识链接  

[如何协同？](https://gitlab.com/101camp/11py/tasks/-/wikis/How2/How2Together#%E5%A6%82%E4%BD%95%E5%8D%8F%E5%90%8C)

[git使用全景](https://gitlab.com/101camp/11py/tasks/-/wikis/How2/How2UseGit)

[仓库文件存放手册](https://gitlab.com/101camp/11py/tasks/-/wikis/HandBooks/HbUsageRepositoryFile)
    
 
